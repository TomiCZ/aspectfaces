/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.composition;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.TestCase;
import com.codingcrayons.aspectfaces.configuration.Configuration;
import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.configuration.Mapping;
import com.codingcrayons.aspectfaces.configuration.Settings;
import com.codingcrayons.aspectfaces.exceptions.AnnotationDescriptorNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.AnnotationNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.AnnotationNotRegisteredException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationNotSetException;
import com.codingcrayons.aspectfaces.exceptions.EvaluatorException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileAccessException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileNotFoundException;
import com.codingcrayons.aspectfaces.metamodel.JavaInspector;
import com.codingcrayons.aspectfaces.metamodel.MetaProperty;
import com.codingcrayons.aspectfaces.support.model.Administrator;
import com.codingcrayons.aspectfaces.util.Files;
import com.codingcrayons.aspectfaces.variableResolver.TagParserException;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class LayoutTest extends TestCase {

	@Test
	public void layoutEL() throws Exception {

		String l1 = layoutInitTemplate("@fieldName@", "@", "@", "/gallery/layouts/administrator-layout.xhtml");
		String l2 = layoutInitTemplate("@{fieldName}", "@{", "}", "/gallery/layouts/administrator-layout2.xhtml");
		String lExpected = Files.readInputStream(this.getClass().getResourceAsStream(
			"/gallery/layouts/expected-result-administrator-layout.xhtml"));
		assertEquals(l1, l2);
		assertEquals(l1, lExpected);
	}

	public void layoutBoundaryLong() throws AnnotationDescriptorNotFoundException, EvaluatorException,
		AnnotationNotRegisteredException, AnnotationNotFoundException, TemplateFileNotFoundException,
		TemplateFileAccessException, ConfigurationNotSetException, TagParserException {

	}

	private String layoutInitTemplate(final String template, String startBoundary, String endBoundary, String layoutPath)
		throws AnnotationDescriptorNotFoundException, EvaluatorException, AnnotationNotRegisteredException,
		AnnotationNotFoundException, TemplateFileNotFoundException, TemplateFileAccessException,
		ConfigurationNotSetException, TagParserException {
		JavaInspector inspector = new JavaInspector(Administrator.class);
		Context context = new Context();

		AFWeaver.registerAllAnnotations();
		AFWeaver.setOpenVariableBoundaryIdentifier(startBoundary);
		AFWeaver.setCloseVariableBoundaryIdentifier(endBoundary);

		// set config (ignores fields)
		Configuration configuration = new Configuration("NAME") {
			@Override
			public String getAbsolutePath() {
				return "";
			}

			@Override
			protected StringBuilder getTemplate(String fileTemplatePath, String prefix)
				throws TemplateFileNotFoundException {
				if (fileTemplatePath.contains("layout")) {
					InputStream is = this.getClass().getResourceAsStream(fileTemplatePath);
					String tagContent = "";
					try {
						tagContent = Files.readInputStream(is);
					} catch (Exception e) {
						e.printStackTrace();
					}
					// add to cache
					return new StringBuilder(tagContent);
				} else {
					return new StringBuilder(template);
				}
			}

			@Override
			protected String getDelimiter() {
				return File.separator;
			}
		};
		configuration.addMapping(new Mapping("String", "path"));
		configuration.addMapping(new Mapping("Long", "path"));
		configuration.addMapping(new Mapping("Owner", "path"));
		configuration.addMapping(new Mapping("Integer", "path"));
		Settings settings = new Settings();
		configuration.setSettings(settings);
		context.setConfiguration(configuration);
		context.setOrderAnnotation("com.codingcrayons.aspectfaces.annotations.UiFormOrder");

		List<MetaProperty> metaProperties = inspector.inspect(context);
		UIFragmentComposer composer = new UIFragmentComposer();
		composer.addAllFields(metaProperties);
		context.setLayout(layoutPath);
		String out = composer.composeForm(context).getCompleteForm().toString();

		return out;
	}
}
