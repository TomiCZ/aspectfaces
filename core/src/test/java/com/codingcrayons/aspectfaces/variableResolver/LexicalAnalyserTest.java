/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.variableResolver;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class LexicalAnalyserTest {

	private void miniReadSymbolsTemplate(String template, String open, String close, String... tokens) {
		try {
			LexicalAnalyser lexanal = new LexicalAnalyser(template, open, close);
			for (String token : tokens) {
				lexanal.readSymbol();
				assertEquals(lexanal.getLexicalSymbolContent().toString(), token);
			}
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.END, lexanal.getLexicalSymbol());
		} catch (TagParserException ex) {
			fail("An unexpected exception:" + ex.getMessage());
		}
	}

	@Test
	public void testMiniReadSymbols() {
		miniReadSymbolsTemplate("<util:inputText label=\"#{text['$entityBean$.$field$']}\"\n />", "$", "$",
			"<util:inputText label=\"#{text['", "entityBean", ".", "field", "']}\"\n />");
	}

	@Test
	public void testMiniReadSymbolsLong() {
		miniReadSymbolsTemplate("<util:inputText label=\"#{text['@{entityBean}.@{field}']}\"\n />", "@{", "}",
			"<util:inputText label=\"#{text['", "entityBean", ".", "field", "']}\"\n />");
	}

	@Test
	public void testMiniReadSymbolsEscape() {
		miniReadSymbolsTemplate("<util:inputText label=\"#{text['\\$entityBean\\$.$field$']}\"\n />", "$", "$",
			"<util:inputText label=\"#{text['$entityBean$.", "field", "']}\"\n />");
	}

	@Test
	public void testMiniReadSymbolsLongEscape() {
		miniReadSymbolsTemplate("<util:inputText label=\"#{text['\\@{entityBean}.@{field}']}\"\n />", "@{", "}",
			"<util:inputText label=\"#{text['@{entityBean}.", "field", "']}\"\n />");
	}

	@Test
	public void testMiniReadSymbolsEscape2() {
		miniReadSymbolsTemplate("<util:inputText label=\"#{text['$entity\\$Bean$.$field$']}\"\n />", "$", "$",
			"<util:inputText label=\"#{text['", "entity$Bean", ".", "field", "']}\"\n />");
	}

	@Test
	public void testMiniReadSymbolsLongEscape2() {
		miniReadSymbolsTemplate("<util:inputText label=\"#{text['@{entity@{\\}Bean}.@{field}']}\"\n />", "@{", "}",
			"<util:inputText label=\"#{text['", "entity@{}Bean", ".", "field", "']}\"\n />");
	}

	@Test
	public void testMiniReadSymbolsLongEscape3() {
		miniReadSymbolsTemplate("<util:inputText label=\"#{text['@{entity@{\\}Be@n}.@{field}']}\"\n />", "@{", "}",
			"<util:inputText label=\"#{text['", "entity@{}Be@n", ".", "field", "']}\"\n />");
	}

	@Test
	public void testReadSymbols() {
		String template = "<util:inputText label=\"#{text['$entityBeanLower$.$field$']}\"\n" + "edit=\"#{edit}\"\n"
			+ "value=\"#{$value$}\"\n" + "required=\"$notNull$\"\n" + "size=\"$size$\"\n"
			+ "title=\"#{text['tooltip.$entityBeanLower$.$field$']}\"\n" + "pattern=\"'$javaPattern$'\"\n"
			+ "minlength=\"$minLength$\"\n" + "maxlength=\"$maxLength$\"\n"
			+ "rendered=\"#{empty render$field$ ? 'true' : render$field$}\"\n" + "id=\"#{prefix}$field$\" />";

		try {
			LexicalAnalyser lexanal = new LexicalAnalyser(template, "$", "$");
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals("<util:inputText label=\"#{text['", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("entityBeanLower", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals(".", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("field", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals("']}\"\nedit=\"#{edit}\"\nvalue=\"#{", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("value", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals("}\"\nrequired=\"", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("notNull", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals("\"\nsize=\"", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("size", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals("\"\ntitle=\"#{text['tooltip.", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("entityBeanLower", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals(".", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("field", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals("']}\"\npattern=\"'", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("javaPattern", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals("'\"\nminlength=\"", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("minLength", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals("\"\nmaxlength=\"", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("maxLength", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals("\"\nrendered=\"#{empty render", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("field", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals(" ? 'true' : render", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("field", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals("}\"\nid=\"#{prefix}", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.EVALUABLE, lexanal.getLexicalSymbol());
			assertEquals("field", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.OTHER, lexanal.getLexicalSymbol());
			assertEquals("\" />", lexanal.getLexicalSymbolContent().toString());
			lexanal.readSymbol();
			assertEquals(LexicalAnalyser.LexicalSymbol.END, lexanal.getLexicalSymbol());
		} catch (TagParserException ex) {
			fail("An unexpected exception:" + ex.getMessage());
		}
	}

	/**
	 * Since there is at least one char then we allow it. We should eventually
	 * discuss whether we check what is there, but I suggest no
	 *
	 * @throws TagParserException
	 */
	public void testNewLineInEvaluableBoundary() throws TagParserException {
		String template = "<util:inputText label=\"#{text['$\n$.$field$']}\" />";
		LexicalAnalyser lexanal = new LexicalAnalyser(template, "$", "$");
		lexanal.readSymbol();
		lexanal.readSymbol();
	}

	@Test(expectedExceptions = TagParserException.class)
	public void testEmptyEvaluableBoundary() throws TagParserException {
		String template = "<util:inputText label=\"#{text['$$.$field$']}\" />";
		LexicalAnalyser lexanal = new LexicalAnalyser(template, "$", "$");
		lexanal.readSymbol();
		lexanal.readSymbol();
	}

	@Test(expectedExceptions = TagParserException.class)
	public void testEndTagInEvaluableBoundary() throws TagParserException {
		String template = "<util:inputText label=\"#{text['$";
		LexicalAnalyser lexanal = new LexicalAnalyser(template, "$", "$");
		lexanal.readSymbol();
		lexanal.readSymbol();
	}
}
