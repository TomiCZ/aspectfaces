/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.util;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotSame;

@SuppressWarnings("unchecked")
public class CollectionsTest {

	private List<String> list1;
	private List<String> list2;
	private List<String> list3;
	private String item1 = "item1";
	private String item2 = "item2";

	@BeforeMethod
	public void setUp() {
		list1 = new ArrayList<String>(3) {
			{
				add("1-1");
				add("1-2");
				add("1-3");
			}
		};
		list2 = new ArrayList<String>(2) {
			{
				add("2-1");
				add("2-2");
			}
		};
		list3 = new ArrayList<String>(1) {
			{
				add("3-1");
			}
		};
	}

	@Test
	public void testConcatCommonList() {
		List<String> result = Collections.concatLists(list1, list2, list3);
		list1.addAll(list2);
		list1.addAll(list3);
		assertNotSame(list1, result);
		assertNotSame(list2, result);
		assertNotSame(list3, result);
		assertEquals(list1, result);
	}

	@Test
	public void testConcatNullList() {
		List<String> result = Collections.concatList(list1, null, null);
		assertEquals(list1, result);
		result = Collections.concatList(null);
		assertEquals(java.util.Collections.emptyList(), result);
	}

	@Test
	public void testConcatItemToList() {
		List<String> result = Collections.concatList(list1, item1, item2);
		list1.add(item1);
		list1.add(item2);
		assertNotSame(list1, result);
		assertEquals(list1, result);
	}

	@Test
	public void testConcatNullItemToList() {
		List<String> result = Collections.concatList(list1, item1, null);
		list1.add(item1);
		assertEquals(list1, result);
	}

	@Test
	public void testConcatNothingToList() {
		List<String> result = Collections.concatList(list2);
		assertNotSame(list2, result);
		assertEquals(list2, result);
	}
}
