/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.io.File;

import com.codingcrayons.aspectfaces.exceptions.ConfigurationFileNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationParsingException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNotSame;
import static org.testng.Assert.fail;

public class ConfigurationStorageTest {

	private final String configurationPath = "test/configuration/core/detail.config.xml";
	private ConfigurationStorage storage;

	@BeforeMethod
	public void setUp() {
		storage = ConfigurationStorage.getInstance();
	}

	public void testAddConfigurationByFile() {
		try {
			File file = new File(configurationPath);
			Configuration configuration = new StaticConfiguration(file.getName());
			storage.addConfiguration(configuration, file, true, true);
			assertNotNull(ConfigurationStorage.getInstance().getConfiguration(file.getName()));
		} catch (Exception e) {
			fail("An unexpected exception: " + e.getMessage());
		}
	}

	@Test
	public void testRestart() {
		ConfigurationStorage origin = ConfigurationStorage.getInstance();
		ConfigurationStorage.restart();
		ConfigurationStorage newOne = ConfigurationStorage.getInstance();
		assertNotSame(origin.hashCode(), newOne.hashCode());
	}

	@Test(expectedExceptions = ConfigurationNotFoundException.class)
	public void testConfigurationDoesNotExist() throws ConfigurationNotFoundException,
		ConfigurationFileNotFoundException, ConfigurationParsingException {
		ConfigurationStorage.getInstance().getConfiguration("foo");
	}
}
