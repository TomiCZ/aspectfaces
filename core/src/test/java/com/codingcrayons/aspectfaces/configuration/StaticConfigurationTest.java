/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.net.URL;

import com.codingcrayons.aspectfaces.TestCase;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationNotSetException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileNotFoundException;
import com.codingcrayons.aspectfaces.metamodel.MetaEntity;
import com.codingcrayons.aspectfaces.metamodel.MetaProperty;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class StaticConfigurationTest extends TestCase {

	private Configuration configuration;
	private final String name = "default.xml";

	@BeforeMethod
	public void setUp() {
		configuration = new StaticConfiguration(name);
	}

	@Test
	public void testGetName() {
		assertEquals(configuration.getName(), name);
	}

	@Test
	public void testIsLoadedDefault() {
		assertFalse(configuration.isLoaded());
	}

	@Test
	public void testIsLoaded() {
		configuration.setLoaded(true);
		assertTrue(configuration.isLoaded());
	}

	@Test
	public void testGetDefaultSettings() {
		assertNull(configuration.getSettings());
	}

	@Test
	public void testGetSettings() {
		Settings settings = new Settings();
		configuration.setSettings(settings);
		assertEquals(configuration.getSettings(), settings);
		assertEquals(configuration.getSettings().getRealPath(), System.getProperty("user.dir"));
	}

	@Test
	public void testGetSettingsMyRealPath() {
		String realPath = "/tmp";
		Settings settings = new Settings();
		settings.setRealPath(realPath);
		configuration.setSettings(settings);
		assertEquals(configuration.getSettings(), settings);
		assertEquals(configuration.getSettings().getRealPath(), realPath);
	}

	@Test
	public void testGetRealPath() {
		Settings settings = new Settings();
		configuration.setSettings(settings);
		assertEquals(configuration.getAbsolutePath(), System.getProperty("user.dir"));
	}

	@Test
	public void testGetGalleryDirectory() {
		Settings settings = new Settings();
		settings.setRealPath("foo");
		String expected = "foo//aspectfaces-gallery";
		configuration.setSettings(settings);
		assertEquals(configuration.getGalleryAbsolutePath(), expected);
	}

	@Test(expectedExceptions = TemplateFileNotFoundException.class)
	public void testGetLayoutAsStreamInvalidFile() throws TemplateFileNotFoundException, ConfigurationNotSetException {
		configuration.setSettings(new Settings());
		configuration.getTag("invalidFile");
	}

	@Test(expectedExceptions = TemplateFileNotFoundException.class)
	public void testGetTagAsStreamInvalidFile() throws TemplateFileNotFoundException, ConfigurationNotSetException {
		configuration.setSettings(new Settings());
		configuration.getTag("invalidFile");
	}

	@Test
	public void testGetTagAsStreamNullPath() throws TemplateFileNotFoundException, ConfigurationNotSetException {
		configuration.setSettings(new Settings());
		assertNull(configuration.getTag(null));
	}

	@Test
	public void testGetTagAsStreamEmptyPath() throws TemplateFileNotFoundException, ConfigurationNotSetException {
		configuration.setSettings(new Settings());
		assertNull(configuration.getTag(""));
	}

	@Test
	public void testGetLayoutAsStream() throws TemplateFileNotFoundException, ConfigurationNotSetException {
		Settings settings = new Settings();
		settings.setRealPath(getRealPath());
		settings.setGalleryDirectory("gallery");
		configuration.setSettings(settings);
		configuration.getTag("layouts/car-layout.xhtml");
	}

	@Test
	public void testGetTagAsStreamNull() throws TemplateFileNotFoundException, ConfigurationNotSetException {
		configuration.setSettings(new Settings());
		assertNull(configuration.getTag(new MetaProperty(new MetaEntity("name", "name")), new Context()));
	}

	@Test
	public void testGetHeaderFileAsStream() throws TemplateFileNotFoundException {
		Settings settings = new Settings();
		settings.setRealPath(getRealPath());
		settings.setHeaderFile("gallery/layouts/header.xhtml");
		configuration.setSettings(settings);
		configuration.getHeaderTag();
	}

	@Test(expectedExceptions = TemplateFileNotFoundException.class)
	public void testGetHeaderFileAsStreamNotFound() throws TemplateFileNotFoundException {
		Settings settings = new Settings();
		settings.setHeaderFile("foo");
		configuration.setSettings(settings);
		configuration.getHeaderTag();
	}

	@Test
	public void testGetFooterFileAsStream() throws TemplateFileNotFoundException {
		Settings settings = new Settings();
		settings.setRealPath(getRealPath());
		settings.setFooterFile("gallery/layouts/footer.xhtml");
		configuration.setSettings(settings);
		configuration.getFooterTag();
	}

	@Test(expectedExceptions = TemplateFileNotFoundException.class)
	public void testGetFooterFileAsStreamNotFound() throws TemplateFileNotFoundException {
		Settings settings = new Settings();
		settings.setFooterFile("foo");
		configuration.setSettings(settings);
		configuration.getFooterTag();
	}

	private String getRealPath() {
		URL realPath = getClass().getClassLoader().getResource(".");
		if (realPath == null) {
			throw new RuntimeException("Real path must not be null.");
		}
		return realPath.getFile();
	}
}
