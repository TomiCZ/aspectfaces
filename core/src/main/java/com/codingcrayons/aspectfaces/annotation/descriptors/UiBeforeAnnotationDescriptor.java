/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.annotation.descriptors;

import java.util.ArrayList;
import java.util.List;

import com.codingcrayons.aspectfaces.annotation.AnnotationProvider;
import com.codingcrayons.aspectfaces.annotation.descriptors.util.PropertyExtension;
import com.codingcrayons.aspectfaces.annotation.registration.AnnotationDescriptor;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.VariableJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;

public class UiBeforeAnnotationDescriptor implements AnnotationDescriptor, VariableJoinPoint {

	@Override
	public String getAnnotationName() {
		return "com.codingcrayons.aspectfaces.annotations.UiBefore";
	}

	@Override
	public List<Variable> getVariables(AnnotationProvider annotationProvider) {
		String value = (String) annotationProvider.getValue("value");
		String title = (String) annotationProvider.getValue("title");
		String type = (String) annotationProvider.getValue("type");
		PropertyExtension pe = new PropertyExtension(type, title, value);

		List<Variable> variables = new ArrayList<Variable>();
		variables.add(new Variable("before", pe));
		return variables;
	}
}
