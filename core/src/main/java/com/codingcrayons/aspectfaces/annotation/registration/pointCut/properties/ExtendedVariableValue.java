/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties;

import com.codingcrayons.aspectfaces.util.Strings;

public class ExtendedVariableValue {
	// delegates all request
	private final Variable variable;

	public ExtendedVariableValue(Variable variable) {
		this.variable = variable;
	}

	final public Object getValue() {
		if (variable == null) {
			return null;
		} else {
			return variable.getValue();
		}
	}

	final public String firstToUpper() {
		return Strings.upperFirstLetter(toString());
	}

	final public String firstToLower() {
		return Strings.lowerFirstLetter(toString());
	}

	final public String escapeExpression() {
		return toString().replace("\\", "\\\\\\\\");
	}

	final public ExtendedVariableValue shortClassName() {
		int indexOfClass = toString().lastIndexOf(".");
		if (indexOfClass == -1) {
			return this;
		} else {
			Variable newVar = new Variable(toString(), toString().substring(indexOfClass + 1));
			return new ExtendedVariableValue(newVar);
		}
	}

	@Override
	final public String toString() {
		return getValue().toString();
	}

	@Override
	final public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ExtendedVariableValue other = (ExtendedVariableValue) obj;
		if ((this.getValue() == null) ? (other.getValue() != null) : !this.getValue().equals(other.getValue())) {
			return false;
		}
		return true;
	}
}
