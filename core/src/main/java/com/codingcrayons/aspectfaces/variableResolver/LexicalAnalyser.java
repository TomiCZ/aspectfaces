/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.variableResolver;

public class LexicalAnalyser {

	// delimets what we want to interpret
	private final String startBoundary;
	private final String endBoundary;
	private final char escapeSymbol = '\\';

	// index to currently matching char
	private int startBoundaryIndex = 0;
	private int endBoundaryIndex = 0;

	// indicates escape
	private boolean escape = false;
	private boolean openBoundary = false;

	private TagInput tagInput = null;

	// what we evaluate
	private char inputChar;

	// how to judge the content
	private InputSymbol symbol;

	// output
	private LexicalSymbol lexicalSymbol;
	private StringBuilder lexicalSymbolContent = new StringBuilder();

	public enum LexicalSymbol {
		EVALUABLE, OTHER, END;

		boolean isEvaluable() {
			return this == EVALUABLE;
		}

		boolean isEnd() {
			return this == END;
		}
	}

	private enum InputSymbol {
		LETTER, NUMBER, OTHER, NEW_LINE, WHITE_SPACE, END_OF_TAG, OPEN_EVALUABLE_BOUNDARY, CLOSE_EVALUABLE_BOUNDARY;

		boolean isOpenEvalBoundary() {
			return this == OPEN_EVALUABLE_BOUNDARY;
		}

		boolean isCloseEvalBoundary() {
			return this == CLOSE_EVALUABLE_BOUNDARY;
		}

		boolean isEndOfTag() {
			return this == END_OF_TAG;
		}

		boolean isNewLine() {
			return this == NEW_LINE;
		}
	}

	private LexicalAnalyser(String startBoundary, String endBoundary) {
		this.startBoundary = startBoundary;
		this.endBoundary = endBoundary;
	}

	public LexicalAnalyser(StringBuilder input, String startBoundary, String endBoundary) throws TagParserException {
		this(startBoundary, endBoundary);
		this.tagInput = new TagInput(input);
		readInput();
	}

	public LexicalAnalyser(String input, String startBoundary, String endBoundary) throws TagParserException {
		this(startBoundary, endBoundary);
		this.tagInput = new TagInput(input);
		readInput();
	}

	/*
	 * As now we do not need such fine grained tokens. tt only takes longer time
	 * - for now commented - uncomment once needed
	 */
	private void readInput() throws TagParserException {
		if (tagInput.hasNext()) {
			inputChar = tagInput.readChar();
			// add here more tokens if necessary

			if (!openBoundary && startBoundary.charAt(startBoundaryIndex) == inputChar) {
				startBoundaryIndex++;
				if (startBoundaryIndex == startBoundary.length()) {
					startBoundaryIndex = 0;
					if (!escape) {
						symbol = InputSymbol.OPEN_EVALUABLE_BOUNDARY;
						lexicalSymbolContent.delete(lexicalSymbolContent.length() + 1 - startBoundary.length(),
							lexicalSymbolContent.length());
						openBoundary = true;
						return;
					} else {
						// remove escape
						lexicalSymbolContent.deleteCharAt(lexicalSymbolContent.length() - startBoundary.length());
						escape = false;
					}
				}
			} else if (openBoundary && endBoundary.charAt(endBoundaryIndex) == inputChar) {
				endBoundaryIndex++;
				if (endBoundaryIndex == endBoundary.length()) {
					endBoundaryIndex = 0;
					if (!escape) {
						symbol = InputSymbol.CLOSE_EVALUABLE_BOUNDARY;
						lexicalSymbolContent.delete(lexicalSymbolContent.length() + 1 - endBoundary.length(),
							lexicalSymbolContent.length());
						openBoundary = false;
						return;
					} else {
						// remove escape
						lexicalSymbolContent.deleteCharAt(lexicalSymbolContent.length() - endBoundary.length());
						escape = false;
					}
				}
			} else if (escape) {
				escape = false;
			}
			if (inputChar == escapeSymbol) {
				escape = true;
			}
			symbol = InputSymbol.OTHER;
		} else {
			symbol = InputSymbol.END_OF_TAG;
		}
	}

	public final void readSymbol() throws TagParserException {
		// }
		switch (symbol) {
			case END_OF_TAG:
				lexicalSymbol = LexicalSymbol.END;
				break;
			case OTHER:
			case NUMBER:
			case WHITE_SPACE:
			case NEW_LINE:
			case LETTER:
				lexicalSymbolContent = new StringBuilder();
				while (!symbol.isOpenEvalBoundary() && !symbol.isEndOfTag()) {
					lexicalSymbolContent.append(inputChar);
					readInput();
				}
				lexicalSymbol = LexicalSymbol.OTHER;

				break;
			case OPEN_EVALUABLE_BOUNDARY:
				lexicalSymbolContent = new StringBuilder();
				readInput();
				if (symbol == InputSymbol.CLOSE_EVALUABLE_BOUNDARY) {
					throw new TagParserException(tagInput, "Evaluable boundary empty");
				} else {
					while (!symbol.isCloseEvalBoundary() && !symbol.isEndOfTag() && !symbol.isNewLine()) {
						lexicalSymbolContent.append(inputChar);
						readInput();
					}
					if (symbol.isNewLine()) {
						throw new TagParserException(tagInput, "Evaluable boundary (" + startBoundary + ") contains "
							+ "<NEW_LINE> near \"" + lexicalSymbolContent + "\"");
					}
					if (symbol.isEndOfTag()) {
						throw new TagParserException(tagInput, "Evaluable boundary (" + startBoundary + ") reached "
							+ "<END_OF_TAG> near \"" + lexicalSymbolContent + "\"");
					}
					lexicalSymbol = LexicalSymbol.EVALUABLE;
					// get ready for next
					readInput();
				}
				break;
			case CLOSE_EVALUABLE_BOUNDARY:
				throw new TagParserException(tagInput, endBoundary + " reached without closing tag");
		}
	}

	public final LexicalSymbol getLexicalSymbol() {
		return lexicalSymbol;
	}

	public final StringBuilder getLexicalSymbolContent() {
		return lexicalSymbolContent;
	}
}
