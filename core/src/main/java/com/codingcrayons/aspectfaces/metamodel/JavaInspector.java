/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.metamodel;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.codingcrayons.aspectfaces.annotation.AnnotationProvider;
import com.codingcrayons.aspectfaces.annotations.UiUseTag;
import com.codingcrayons.aspectfaces.cache.ResourceCache;
import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.exceptions.AnnotationNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.AnnotationNotRegisteredException;
import com.codingcrayons.aspectfaces.exceptions.EvaluatorException;

public class JavaInspector implements Inspector {

	private final List<Class<?>> classes;

	public JavaInspector(Class<?> clazz) {
		this(new Class<?>[]{clazz});
	}

	public JavaInspector(Class<?> classes[]) {
		this.classes = new ArrayList<Class<?>>(classes.length + 2);
		for (Class<?> clazz : classes) {
			this.classes.add(clazz);
		}
	}

	public void addClass(Class<?> clazz) {
		this.classes.add(clazz);
	}

	@Override
	public List<MetaProperty> inspect(Context context)
		throws EvaluatorException, AnnotationNotRegisteredException, AnnotationNotFoundException {

		List<MetaProperty> metaProperties = new ArrayList<MetaProperty>();
		int index = 1; // class order
		for (Class<?> clazz : this.classes) {
			// create metaEntity to share among metaProperties
			MetaEntity metaEntity = new MetaEntity(clazz.getName(), clazz.getSimpleName(), index++);

			for (Method method : JavaInspector.getGetters(clazz)) {

				MetaPropertyBuilder builder = new JavaMetaPropertyBuilder(context, metaEntity, method);

				if (!builder.isMetaPropertyApplicable()) {
					// Save unnecessary reflection
					continue;
				}

				for (Annotation annotation : getMethodAnnotations(clazz, method)) {
					if (!builder.isMetaPropertyApplicable()) {
						// Save unnecessary reflection
						break;
					}
					AnnotationProvider annotationProvider = new AnnotationProvider(annotation.annotationType()
						.getName());
					for (Method annotationMethod : getAnnotationMethods(clazz, method, annotation.annotationType())) {
						try {
							annotationProvider
								.setValue(annotationMethod.getName(), annotationMethod.invoke(annotation));
						} catch (IllegalArgumentException e) {
							throw new AnnotationNotFoundException("Annotation: "
								+ annotation.annotationType().getName(), e);
						} catch (IllegalAccessException e) {
							throw new AnnotationNotFoundException("Annotation: "
								+ annotation.annotationType().getName(), e);
						} catch (InvocationTargetException e) {
							throw new AnnotationNotFoundException("Annotation: "
								+ annotation.annotationType().getName(), e);
						}
					}
					builder.addAnnotation(annotationProvider);
				}

				UiUseTag uiUseTagAnnotation = method.getAnnotation(UiUseTag.class);
				if (uiUseTagAnnotation != null) {
					builder.setSpecificTag(uiUseTagAnnotation.value());
				}

				// includes the actual before and after properties
				metaProperties.addAll(builder.getMetaProperties());
			}
		}

		return metaProperties;
	}

	/**
	 * @param clazz
	 * @return List for all getters (get and is methods) in class
	 */
	private static List<Method> getGetters(Class<?> clazz) {
		String cacheKey = clazz.getName();

		List<Method> cached = ResourceCache.getInstance().getClassMethods(cacheKey);

		if (cached == null) {
			List<Method> getters = new ArrayList<Method>();
			for (Method method : clazz.getMethods()) {
				if (method.getName().startsWith("get") || method.getName().startsWith("is")) {
					getters.add(method);
				}
			}
			ResourceCache.getInstance().putClassMethods(cacheKey, getters);
			// keep the method here in case we disable cache
			return getters;
		}
		return cached;
	}

	private Annotation[] getMethodAnnotations(Class<?> originatingClass, Method method) {
		String cacheKey = originatingClass.getName() + method.getName();

		Annotation[] cached = ResourceCache.getInstance().getMethodAnnotations(cacheKey);
		if (cached == null) {
			Annotation[] annotations = method.getAnnotations();
			ResourceCache.getInstance().putMethodAnnotations(cacheKey, annotations);
			// keep the method here in case we disable cache
			return annotations;
		}
		return cached;
	}

	private static List<Method> getAnnotationMethods(Class<?> originatingClass, Method originatingMethod,
													 Class<? extends Annotation> annotation) {
		String cacheKey = originatingClass.getName() + originatingMethod.getName() + annotation.getName();

		List<Method> cached = ResourceCache.getInstance().getAnnotationMethods(cacheKey);
		if (cached == null) {
			List<Method> methods = new ArrayList<Method>();
			for (Method method : annotation.getMethods()) {
				if (!method.getName().startsWith("equals") && !method.getName().startsWith("hashCode")
					&& !method.getName().startsWith("toString") && !method.getName().startsWith("annotationType")) {
					methods.add(method);
				}
			}
			ResourceCache.getInstance().putAnnotationMethods(cacheKey, methods);
			// keep the method here in case we disable cache
			return methods;
		}
		return cached;
	}
}
