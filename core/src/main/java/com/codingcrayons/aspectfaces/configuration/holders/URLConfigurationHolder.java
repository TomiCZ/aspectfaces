/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration.holders;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;

import com.codingcrayons.aspectfaces.configuration.Configuration;
import com.codingcrayons.aspectfaces.configuration.parsers.ConfigurationParser;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationFileNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationParsingException;

public class URLConfigurationHolder extends ConfigurationHolder {

	private final URL url;

	public URLConfigurationHolder(Configuration configuration, URL url, boolean lazyInitialize,
								  boolean checkModification) throws ConfigurationFileNotFoundException, ConfigurationParsingException {
		this(configuration, url, lazyInitialize, checkModification, null);
	}

	public URLConfigurationHolder(Configuration configuration, URL url, boolean lazyInitialize,
								  boolean checkModification, ConfigurationParser parser)
		throws ConfigurationFileNotFoundException, ConfigurationParsingException {

		super(configuration, checkModification, parser);
		this.url = url;
		if (!lazyInitialize) {
			this.loadConfiguration();
		}
	}

	@Override
	protected boolean configurationFileModified() throws ConfigurationFileNotFoundException {
		try {
			if (url.openConnection().getLastModified() > loadedTime) {
				return true;
			}
		} catch (IOException e) {
			throw new ConfigurationFileNotFoundException("Problem with configuration " + url.getFile() + " occured.", e);
		}
		return false;
	}

	@Override
	protected final void loadConfiguration() throws ConfigurationFileNotFoundException, ConfigurationParsingException {
		InputStream in;
		try {
			in = this.url.openStream();
		} catch (IOException e) {
			throw new ConfigurationFileNotFoundException("Problem with configuration " + url.getFile() + " occured.", e);
		}
		this.loadedTime = Calendar.getInstance().getTime().getTime();
		this.getParser().parseConfiguration(in, configuration);
		this.configuration.setLoaded(true);
	}
}
