/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration.guards;

import javax.el.ValueExpression;

import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.el.AFContext;
import com.codingcrayons.aspectfaces.el.ELUtil;
import com.codingcrayons.aspectfaces.metamodel.MetaProperty;

public class ConditionGuard extends AbstractGuard {

	private final String expression;

	public ConditionGuard(String expression, String tagPath, String[] profiles, String[] userRoles) {
		super(tagPath, profiles, userRoles);
		this.expression = expression;
	}

	@Override
	public boolean evaluate(MetaProperty metaProperty, Context context, AFContext variableContext) {
		if (!this.evaluateRoles(context)) {
			return false;
		}

		ValueExpression e = new ELUtil().createValueExpression(variableContext, this.expression, boolean.class);

		try {
			return (Boolean) e.getValue(variableContext);
		} catch (Exception ex) {
			return false;
		}
	}
}
