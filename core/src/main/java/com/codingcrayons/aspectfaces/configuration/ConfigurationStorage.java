/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.io.File;
import java.io.Serializable;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import com.codingcrayons.aspectfaces.configuration.holders.FileConfigurationHolder;
import com.codingcrayons.aspectfaces.configuration.holders.IConfigurationHolder;
import com.codingcrayons.aspectfaces.configuration.holders.URLConfigurationHolder;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationFileNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationParsingException;

public class ConfigurationStorage implements Serializable {

	private static final long serialVersionUID = -8478446389490871473L;
	private final Map<String, IConfigurationHolder> configurationHolders;
	private static final ReentrantLock lock = new ReentrantLock();
	private static ConfigurationStorage instance;

	/**
	 * Drop current instance (if exists) and create new ConfigurationStorage.
	 */
	public static void restart() {
		instance = new ConfigurationStorage();
	}

	/**
	 * @return ConfigurationStorage singleton instance
	 */
	public static ConfigurationStorage getInstance() {
		if (instance == null) {
			lock.lock();
			try {
				if (instance == null) {
					instance = new ConfigurationStorage();
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}

	private ConfigurationStorage() {
		this.configurationHolders = new HashMap<String, IConfigurationHolder>(4);
	}

	public void addConfiguration(Configuration configuration, URL url, boolean lazyInitialize, boolean checkModification)
		throws ConfigurationFileNotFoundException, ConfigurationParsingException {

		configurationHolders.put(configuration.getName(), new URLConfigurationHolder(configuration, url,
			lazyInitialize, checkModification));
	}

	public void addConfiguration(Configuration configuration, File file, boolean lazyInitialize,
								 boolean checkModification)
		throws ConfigurationParsingException, ConfigurationFileNotFoundException {

		configurationHolders.put(configuration.getName(), new FileConfigurationHolder(configuration, file,
			lazyInitialize, checkModification));
	}

	public Configuration getConfiguration(String name) throws ConfigurationNotFoundException,
		ConfigurationFileNotFoundException, ConfigurationParsingException {

		IConfigurationHolder configurationHolder = this.configurationHolders.get(name);
		if (configurationHolder == null) {
			throw new ConfigurationNotFoundException("Configuration with name: '" + name + "' not found in "
				+ "configuration container.");
		}
		return configurationHolder.getConfiguration();
	}
}
