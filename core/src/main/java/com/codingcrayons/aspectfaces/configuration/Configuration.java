/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.codingcrayons.aspectfaces.annotation.AnnotationProvider;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationNotSetException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileNotFoundException;
import com.codingcrayons.aspectfaces.metamodel.MetaProperty;
import com.codingcrayons.aspectfaces.util.Strings;

abstract public class Configuration {
	private static final String[] permanentIgnoringAnnotations = {
		"com.codingcrayons.aspectfaces.annotations.UiIgnore"
	};
	private static List<String> defaultIgnoreFields = new ArrayList<String>();
	private static List<String> defaultIgnoringAnnotations = new ArrayList<String>();

	static {
		Configuration.initDefaultIgnoringAnnotations();
	}

	/**
	 * Initialize permanent ignoring annotation
	 */
	private static void initDefaultIgnoringAnnotations() {
		// @UiIgnore
		Configuration.defaultIgnoringAnnotations.addAll(Arrays.asList(Configuration.permanentIgnoringAnnotations));
	}

	/**
	 * Adds default ignoreField
	 *
	 * @param ignoreField - ignoreField
	 */
	public static void addDefaultIgnoreField(String ignoreField) {
		Configuration.defaultIgnoreFields.add(ignoreField);
	}

	/**
	 * Removes all items from default ignoreFields
	 */
	public static void clearDefaultIgnoreFields() {
		Configuration.defaultIgnoreFields.clear();
	}

	/**
	 * Adds default ignoringAnnotation
	 *
	 * @param ignoringAnnotation - ignoringAnnotation
	 */
	public static void addDefaultIgnoringAnnotation(String ignoringAnnotation) {
		Configuration.defaultIgnoringAnnotations.add(ignoringAnnotation);
	}

	/**
	 * Remove all items from default ignoringAnnotations
	 */
	public static void clearDefaultIgnoringAnnotations() {
		Configuration.defaultIgnoringAnnotations.clear();
		Configuration.initDefaultIgnoringAnnotations();
	}

	/**
	 * Remove all items from default ignoreFields and ignoringAnnotations
	 */
	public static void reset() {
		Configuration.clearDefaultIgnoreFields();
		Configuration.clearDefaultIgnoringAnnotations();
	}

	protected Settings settings;
	protected Map<String, Mapping> mappings;
	protected Set<String> ignoreFields = new HashSet<String>();
	protected Set<String> ignoringAnnotations = new HashSet<String>();
	protected boolean loaded;
	protected final String name;

	public Configuration(String name) {
		this.name = name;
		this.mappings = new LinkedHashMap<String, Mapping>();
		this.loaded = false;
		resetIgnoreFields();
		resetIgnoringAnnotations();
	}

	public boolean isLoaded() {
		return this.loaded;
	}

	public final void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public Settings getSettings() {
		return this.settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	public void addMapping(Mapping mapping) {
		this.mappings.put(mapping.getName(), mapping);
	}

	/**
	 * Adds ignoreField to configuration
	 *
	 * @param ignoreField - ignoreFiled
	 */
	public void addIgnoreField(String ignoreField) {
		this.ignoreFields.add(ignoreField);
	}

	/**
	 * @return list of Ignore Fields
	 */
	public Set<String> getIgnoreFields() {
		return ignoreFields;
	}

	/**
	 * @return list of IgnoreField
	 */
	public Set<String> getIgnoringAnnotations() {
		return ignoringAnnotations;
	}

	/**
	 * Removes all items from ignoreFields
	 */
	public void resetIgnoreFields() {
		this.ignoreFields.clear();
		this.ignoreFields.addAll(Configuration.defaultIgnoreFields);
	}

	/**
	 * Adds ignoringAnnotation to configuration
	 *
	 * @param ignoringAnnotation - ignoringAnnotations
	 */
	public void addIgnoringAnnotations(String ignoringAnnotation) {
		this.ignoringAnnotations.add(ignoringAnnotation);
	}

	/**
	 * Removes all items from ignoringAnnotations
	 */
	public void resetIgnoringAnnotations() {
		this.ignoringAnnotations.clear();
		this.ignoringAnnotations.addAll(Arrays.asList(Configuration.permanentIgnoringAnnotations));
		this.ignoringAnnotations.addAll(Configuration.defaultIgnoringAnnotations);
	}

	public String getEvaluatorClassName() {
		return this.settings.getEvaluatorClassName();
	}

	/**
	 * @return tagPath or null, when no tag for this field is specified
	 */
	public String getTagPath(MetaProperty metaProperty, Context context) {
		Mapping mapping = this.mappings.get(metaProperty.getReturnType());
		return mapping != null ? mapping.getTagPath(metaProperty, context) : null;
	}

	private boolean isIgnoredField(String fieldName) {
		return this.ignoreFields.contains(fieldName);
	}

	/**
	 * @param annotationProvider - annotation provider
	 * @return true if the specified annotationName equals some of ignoringAnnotations
	 */
	public final boolean isIgnoringAnnotation(AnnotationProvider annotationProvider) {
		return this.ignoringAnnotations.contains(annotationProvider.getName());
	}

	/**
	 * Control practicability of requested field
	 *
	 * @param returnType
	 * @param fieldName
	 * @return if field is not in ignore list and is defined as mapping
	 */
	public final boolean canBeFieldProcessed(String returnType, String fieldName) {
		return !(isIgnoredField(fieldName) || !this.mappings.containsKey(returnType));
	}

	/**
	 * Returns variables defined for the specified meta property return type in a configuration mapping
	 *
	 * @param metaProperty - meta property
	 * @return variables, or null if there is no configuration mapping for the specified meta property return type
	 */
	public List<Variable> getDefaultVariables(MetaProperty metaProperty) {
		if (this.canBeFieldProcessed(metaProperty.getReturnType(), metaProperty.getName())) {
			return this.mappings.get(metaProperty.getReturnType()).getVariables();
		}
		return null;
	}

	public String getName() {
		return this.name;
	}

	public StringBuilder getHeaderTag() throws TemplateFileNotFoundException {
		String fileTemplatePath = this.getSettings().getHeaderFile();
		return getTemplate(fileTemplatePath, "");
	}

	public StringBuilder getFooterTag() throws TemplateFileNotFoundException {
		String fileTemplatePath = this.getSettings().getFooterFile();
		return getTemplate(fileTemplatePath, "");
	}

	public StringBuilder getTag(String fileTemplatePath) throws TemplateFileNotFoundException,
		ConfigurationNotSetException {
		if (Strings.isBlank(this.getSettings().getGalleryDirectory())) {
			throw new ConfigurationNotSetException("Configuration does not specify gallery path");
		}
		// do not use File.separator for win
		return getTemplate(fileTemplatePath, getDelimiter() + this.getSettings().getGalleryDirectory());
	}

	public StringBuilder getTag(MetaProperty metaProperty, Context context) throws TemplateFileNotFoundException,
		ConfigurationNotSetException {
		String tagPath = this.getTagPath(metaProperty, context);
		return getTag(tagPath);
	}

	public String getGalleryAbsolutePath() {
		return getAbsolutePath() + getDelimiter() + this.getSettings().getGalleryDirectory();
	}

	public abstract String getAbsolutePath();

	protected abstract StringBuilder getTemplate(String fileTemplatePath, String prefix)
		throws TemplateFileNotFoundException;

	protected abstract String getDelimiter();
}
