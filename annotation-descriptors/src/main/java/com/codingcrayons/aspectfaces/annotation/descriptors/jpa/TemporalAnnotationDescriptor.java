/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.annotation.descriptors.jpa;

import java.util.ArrayList;
import java.util.List;

import com.codingcrayons.aspectfaces.annotation.AnnotationProvider;
import com.codingcrayons.aspectfaces.annotation.registration.AnnotationDescriptor;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.VariableJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;

public class TemporalAnnotationDescriptor implements AnnotationDescriptor, VariableJoinPoint {

	@Override
	public String getAnnotationName() {
		return "javax.persistence.Temporal";
	}

	@Override
	public List<Variable> getVariables(AnnotationProvider annotationProvider) {
		List<Variable> variables = new ArrayList<Variable>();
		variables.add(new Variable("temporal", true));

		// there is the value type of javax.persistence.TemporalType which is enum
		// the enum has #toString() defined as #name()
		// the name returns the exact name of the enum instance which is one of TIMESTAMP, DATE, TIME
		// that means, that the value.toString().toLowerCase returns "timestamp" or "date" or time"
		String temporalType = annotationProvider.getValue("value").toString().toLowerCase();

		variables.add(new Variable("temporalType", temporalType));
		return variables;
	}
}
