/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.plugins.j2ee;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.annotation.registration.AnnotationDescriptor;
import com.codingcrayons.aspectfaces.plugins.j2ee.configuration.ServerConfiguration;
import com.codingcrayons.aspectfaces.plugins.j2ee.exceptions.ConfigurationFileNotFoundException;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AspectFacesListener implements ServletContextListener {

	private static Logger LOGGER = LoggerFactory.getLogger(AspectFacesListener.class);

	@Override
	public void contextInitialized(ServletContextEvent event) {

		ServletContext context = event.getServletContext();

		InputStream in = null;

		try {
			in = context.getResourceAsStream("/WEB-INF/aspectfaces-config.xml");
			if (in == null) {
				throw new ConfigurationFileNotFoundException("aspectfaces-config.xml not found in /WEB-INF/");
			}
			SAXReader reader = new SAXReader();
			Document document = reader.read(in);

			Element root = document.getRootElement();

			@SuppressWarnings("rawtypes")
			Iterator iterator = root.elementIterator();
			while (iterator.hasNext()) {
				Element element = (Element) iterator.next();
				if (element.getName().equals("configurations-registration")) {

					@SuppressWarnings("rawtypes")
					Iterator crIterator = element.elementIterator();
					while (crIterator.hasNext()) {
						Element crElement = (Element) crIterator.next();
						if (crElement.getName().equals("configuration")) {
							String name = null;
							String path = null;
							boolean checkModification = false;
							boolean lazyLoad = false;
							@SuppressWarnings("rawtypes")
							Iterator attributeIterator = crElement.attributeIterator();
							while (attributeIterator.hasNext()) {
								Attribute attribute = (Attribute) attributeIterator.next();
								if (attribute.getName().equals("name")) {
									name = attribute.getValue();
								} else if (attribute.getName().equals("path")) {
									path = attribute.getValue();
								} else if (attribute.getName().equals("check-modification")) {
									if (attribute.getValue().equals("true")) {
										checkModification = true;
									}
								} else if (attribute.getName().equals("lazy-load")) {
									if (attribute.getValue().equals("true")) {
										lazyLoad = true;
									}
								}
							}
							if (name != null && path != null) {
								URL config = context.getResource(path);
								if (config == null) {
									LOGGER.error("Configuration not found on path: '{}'.", path);
									throw new RuntimeException("Configuration not found on path: " + path);
								}
								AFWeaver.addConfiguration(new ServerConfiguration(name, context), config, lazyLoad, checkModification);
							}
						}
					}
				}
				if (element.getName().equals("annotations-registration")) {
					@SuppressWarnings("rawtypes")
					Iterator arIterator = element.elementIterator();
					while (arIterator.hasNext()) {
						Element arElement = (Element) arIterator.next();
						if (arElement.getName().equals("name")) {
							AFWeaver.registerAnnotation((AnnotationDescriptor) Class.forName(
								arElement.getTextTrim()).newInstance());
						}
					}
				}
			}

			// load properties
			AFWeaver.init();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				// ignore since commons io does the same
				// See org.apache.commons.io.IOUtils.java#IOUtils.closeQuietly
				LOGGER.error("Closing aspectfaces-config.xml file stream", e);
			}
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}
}
